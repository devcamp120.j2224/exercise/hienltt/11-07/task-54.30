package com.devcamp.s50.task5430.restapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestApiApplication.class, args);
		// Khai báo đối tượng class cRandomNumber
		CRandomNumber randomNumber = new CRandomNumber();
		// In số ngẫu nhiên kiểu double từ 1-100 ra console
		System.out.println(randomNumber.randomDoubleNumber());
		// In số ngẫu nhiên kiểu int từ 1-10 ra console
		System.out.println(randomNumber.randomIntNumber());
	}

}
